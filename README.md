# NGINNX default config

That config consist all defaults for nginx out the box 

## Getting Started

Just clone that project.
And copy nginx.conf to your directory with config.

### Prerequisites

You need installed nginx. Check that by:
```
nginx -v
```
And you will see somvethink like that:
```
nginx version: nginx/1.14.0 (Ubuntu)
```
If you haven't nginx, just try:

### Installing nginx 

### for Ubuntu
```
sudo apt update
sudo apt install nginx
```

### for CentOS
```
sudo yum install epel-release
sudo yum install nginx
```

### Installing nginx default config from my repo

Backup you current config

```
sudo mv /etc/nginx/nginx.conf.old
```

Go to your some dir, where you want store a configurations and clone the project to 

```
git clone https://gitlab.com/airfreeforfly/rebrain-devops-task-checkout.git
```

Go to project directory

```
cd rebrain-devops-task-checkout
```

Set nginx config by defaults. Just replace that

```
sudo cp nginx.conf /etc/nginx/nginx.conf 
```


### Reload nginx config 

Without downtime
```
sudo nginx -s reload
```
